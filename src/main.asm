.include "m128def.inc"
.include "macros.asm"
.include "definitions.asm"

.org 0
  jmp reset
.org INT4addr
  jmp	KPD_isr_ext_int4
  jmp	KPD_isr_ext_int5
  jmp	KPD_isr_ext_int6
  jmp	KPD_isr_ext_int7
.org OVF0addr
  jmp timer0
.org OVF2addr
  jmp timer2

.org 0x30

.include "dmx.asm"
.include "kpd.asm"
.include "lcd.asm"
.include "printf.asm"
.include "encoder.asm"

reset:
  ldsp  ramend

  clr c0
  clr c1
  clr c2
  clr a3

  rcall DMX_init
  rcall DMX_reset
  rcall KPD_init
  rcall LCD_init
  rcall encoder_init

  OUTI TIMSK, (1<<TOIE0) ; timer0 overflow
  ; OUTI TIMSK, (1<<TOIE0)+(1<<TOIE2) ; timer0+2 overflow
  OUTI ASSR, (0<<AS0)    ; AS0=0 -> 4MHz clock
  OUTI TCCR0,1           ; CS00=1 CK/1
  OUTI TCCR2,5           ; 

  sei

  jmp main

timer0:
  in _sreg, SREG
  
  push a0
  push b0
  
  mov a0, c0
  call encoder
  cp a0, c0
  breq timer0_no_update
  mov c0, a0
  ldi a3, 1

  timer0_no_update:

  pop b0
  pop a0
  out SREG, _sreg
  reti

timer2:
  in _sreg, SREG
  mov _u, u
  ; do work
  out SREG, _sreg
  mov u, _u
  reti

main:
  rcall check_buttons
  rcall update_buffer
  rcall update_lcd
  call DMX_send_packet
  WAIT_MS 50
  rjmp main       ; jump back to main

check_buttons:
  in u, PIND     ; u : buttons as a byte
  ldi w, 0       ; w : channel offset outputed as c2
  check_buttons_loop:
    sbrc u,0
    rjmp check_buttons_continue
    check_buttons_update:
    mov c2,w
    call DMX_load_value
    ret 

    check_buttons_continue:
    inc w
    lsr u ; u >> 1
    cpi w, 8
    brne check_buttons_loop

  ; no button is pressed = don't update the channel offset
  ret

update_buffer:
  tst c1
  brne update_buffer_static_mode
  mov w, c2
  update_buffer_anim_mode:
    cpi w,0 ; case mode=0
    brne PC+2
    ret
    
    cpi w,1 ; case mode=1
    brne PC+4
    call DMX_buffer_dimm
    ret

    cpi w,2 ; case mode=2
    brne PC+4
    call DMX_reset
    ret

    cpi w,3 ; case mode=3
    brne PC+4
    call DMX_buffer_set_pattern_1
    ret

    cpi w,4 ; case mode=4
    brne PC+4
    call DMX_buffer_set_pattern_2
    ret

    cpi w,5 ; case mode=5
    brne PC+4
    call DMX_buffer_flashes
    ret

    cpi w,6 ; case mode=6
    brne PC+4
    call DMX_buffer_restore
    ret

    cpi w,7 ; case mode=7
    brne PC+4
    call DMX_buffer_save
    ret
    
  ret

  update_buffer_static_mode:
  tst a3
  brne PC+2 ; return if no update
  ret
  clr a3

  call DMX_store_value
  ret

update_lcd:
  tst c1
  brne update_lcd_static_mode

  update_lcd_anim_mode:

  mov a0, c2
  mov b0, c0

  call LCD_home
  PRINTF  LCD
.db     "Program n",FDEC,a,"              ",0
 
  call LCD_lf
  PRINTF  LCD
.db     "Val=",FDEC,b,"             ",0

  ret


  update_lcd_static_mode:

  mov a0, c2
  mov b0, c1

  call LCD_home
  PRINTF  LCD
.db    "Ch=",FDEC,a,"     Val=",FDEC,c,"  ",0

  call LCD_lf
  PRINTF  LCD
.db    "Fixtures=",FDEC,b,"      ",0

  ret