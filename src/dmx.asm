

.equ DMX_BUFFER_LEN = 505
.dseg
dmx_buffer_0:.byte	DMX_BUFFER_LEN
.cseg
.equ dmx_buffer_0_next = dmx_buffer_0 + DMX_BUFFER_LEN

.dseg
dmx_buffer_1:.byte	DMX_BUFFER_LEN
.cseg
.equ dmx_buffer_1_next = dmx_buffer_1 + DMX_BUFFER_LEN

.dseg
dmx_buffer_2:.byte	DMX_BUFFER_LEN
.cseg
.equ dmx_buffer_2_next = dmx_buffer_2 + DMX_BUFFER_LEN

.macro SET_BIT
  sbi  PORTB,0
  cbi  PORTB,1
.endmacro

.macro CLR_BIT
  cbi  PORTB,0
  sbi  PORTB,1
.endmacro

.def reg_dmx_byte = r18 ; a0 = byte from/to the DMX buffer
.def reg_dmx_bit = r19 ; a1 = bit index in DMX frame

DMX_init:
  OUTI  DDRB,0xff  ; output register to i/o Data Direction // DMX 
  call DMX_load_value
  ret

DMX_reset:
  ldi xl,low(dmx_buffer_0)
  ldi xh,high(dmx_buffer_0)
  ldi reg_dmx_byte,0
  reset_loop:
    st x+,reg_dmx_byte     ; store low(X) at X then inc. X
    cpi xh,high(dmx_buffer_0_next)     ; compare if high(X) == 3, i.e. if X = 0x0300
    brne reset_loop
  ret


DMX_load_value:
  call DMX_compute_X
  ld c0,x
  ret

DMX_store_value:
  call DMX_compute_X
  st x, c0            ; BUFFER[light index + channel offset] = value
  ret

; c0 : 0-255 value
; c1 : light index
; c2 : channel offset
; a3 : new value
DMX_compute_X:
  ldi xl,low(dmx_buffer_0+1-8)
  ldi xh,high(dmx_buffer_0+1-8)

  mov u,c1   ; u = light index
  lsl u      ; u = 2 x light index
  lsl u      ; u = 4 x light index
  lsl u      ; u = 8 x light index ; u <= 255

  ADDX u     ; X = 8 x light index
  ADDX c2    ; X = 8 x light index + channel offset

  ret

DMX_buffer_dimm:
  ldi xl,low(dmx_buffer_0+1)
  ldi xh,high(dmx_buffer_0+1)
  DMX_buffer_dimm_loop:
    st x, c0
    ADDI2 xh, xl, 8
    cpi xh,high(dmx_buffer_0_next+1)
    brne DMX_buffer_dimm_loop 
  ret

DMX_buffer_set_pattern_1:
  ldi xl,low(dmx_buffer_0+1)
  ldi xh,high(dmx_buffer_0+1)
  ldi w, 8
  DMX_buffer_set_pattern_1_loop:
    ldi reg_dmx_byte, 0xff
    st x+, reg_dmx_byte      ; D
    ldi reg_dmx_byte, 0x0
    st x+, reg_dmx_byte      ; R
    st x+, reg_dmx_byte      ; G
    st x+, reg_dmx_byte      ; B
    st x+, reg_dmx_byte      ; A
    ldi reg_dmx_byte, 0xff
    st x+, reg_dmx_byte      ; W
    ldi reg_dmx_byte, 0x0
    st x+, reg_dmx_byte      ; UV
    st x+, reg_dmx_byte      ; S
    dec w
    brne DMX_buffer_set_pattern_1_loop
  ret

DMX_buffer_set_pattern_2:
  ldi xl,low(dmx_buffer_0+1)
  ldi xh,high(dmx_buffer_0+1)
  ldi w, 8
  DMX_buffer_set_pattern_2_loop:
    ldi reg_dmx_byte, 0xff
    st x+, reg_dmx_byte      ; D
    ldi reg_dmx_byte, 0x0
    st x+, reg_dmx_byte      ; R
    st x+, reg_dmx_byte      ; G
    st x+, reg_dmx_byte      ; B
    ldi reg_dmx_byte, 0xff
    st x+, reg_dmx_byte      ; A
    ldi reg_dmx_byte, 0x0
    st x+, reg_dmx_byte      ; W
    st x+, reg_dmx_byte      ; UV
    st x+, reg_dmx_byte      ; S
    dec w
    brne DMX_buffer_set_pattern_2_loop
  ret

DMX_buffer_save:
  ; clone buffer 0 to buffer 2
  ldi xl,low(dmx_buffer_0)
  ldi xh,high(dmx_buffer_0)
  ldi yl,low(dmx_buffer_2)
  ldi yh,high(dmx_buffer_2)

  DMX_buffer_save_loop:
    ld reg_dmx_byte, x+
    st y+, reg_dmx_byte
    cpi xh,high(dmx_buffer_0_next)
    brne DMX_buffer_save_loop
  ret

DMX_buffer_restore:
  ; clone buffer 2 to buffer 0
  ldi xl,low(dmx_buffer_2)
  ldi xh,high(dmx_buffer_2)
  ldi yl,low(dmx_buffer_0)
  ldi yh,high(dmx_buffer_0)

  DMX_buffer_restore_loop:
    ld reg_dmx_byte, x+
    st y+, reg_dmx_byte
    cpi xh,high(dmx_buffer_2_next)
    brne DMX_buffer_restore_loop
  ret

DMX_buffer_flashes:
  ldi xl,low(dmx_buffer_0+1)
  ldi xh,high(dmx_buffer_0+1)
  ldi w, 8
  ldi b0, 100
  DMX_buffer_flashes_loop:
    ldi reg_dmx_byte, 0xff
    st x+, reg_dmx_byte      ; D
    ldi reg_dmx_byte, 0x0
    st x+, reg_dmx_byte      ; R
    st x+, reg_dmx_byte      ; G
    st x+, reg_dmx_byte      ; B
    st x+, reg_dmx_byte      ; A
    ldi reg_dmx_byte, 0xff
    st x+, reg_dmx_byte      ; W
    ldi reg_dmx_byte, 0x0
    st x+, reg_dmx_byte      ; UV
    st x+, b0                ; S
    addi b0, 20
    dec w
    brne DMX_buffer_flashes_loop
  ret

DMX_send_packet:
  cli
  push reg_dmx_byte
  push reg_dmx_bit
  push xl
  push xh

  ; BREAK
  CLR_BIT ; 4 cycles
  WAIT_C (22*32-4) ; 22 bits x 32 c/b - 4c = 700c = 87.5us

  ; MARK AFTER BREAK
  SET_BIT
  WAIT_C 62
  ;WAIT_C (2*32-4) ; 2 bits x 32 c/b - 4c = 60c = 8.2us

  ; Buffer processing
  clr  xl        ; set X = 0x0100
  ldi  xh,1

  packet_frame:
    ld reg_dmx_byte,x+     ; load next byte from buffer (+post-incr.) ; 2 cycles
    ldi reg_dmx_bit,8     ; counter for the 8 bits

    packet_frame_start: ; START BIT
    CLR_BIT      ; 4 cycles
    WAIT_C 26    ; wait for 32 - 4 = 28 cycles
    
    packet_frame_bit:
      bst reg_dmx_byte,0   ; Store LSB in T Flag ; 1 cycle
      ror reg_dmx_byte
      brts packet_frame_bit_set ; 2 cycles (1 or 2 but compensated)

      packet_frame_bit_clr:
      CLR_BIT  ; 4 cycles
      WAIT_C 27 ; 22 ; 32 - (1+2+4+1+2) = 22 cycles
      dec reg_dmx_bit  ; 1 cycle
      brne packet_frame_bit ; 2 cycles
      rjmp packet_frame_end ; (2 cycles - end of frame)

      packet_frame_bit_set:
      nop      ; 0 cycle (1 cycle to compensate brts)
      SET_BIT  ; 4 cycles
      WAIT_C 27 ; 22 ; 32 - (1+2+4+1+2) = 22 cycles
      dec reg_dmx_bit  ; 1 cycle
      brne packet_frame_bit ; 2 cycles
      rjmp packet_frame_end ; (2 cycles - end of frame)

    packet_frame_end: ; STOP BITS
    SET_BIT ; 4 cycles
    WAIT_C 58 ; 2x32 - 4 - 2 (rjmp) cycles = 58 cycles

  WAIT_C 60 ; wait between frames
  
  cpi xh,3 ; compare if high(X) == 3, i.e. if X = 0x03?? ~ 0x0300  
  breq packet_end
  jmp packet_frame
  
  packet_end:
  CLR_BIT

  pop xl
  pop xh
  pop reg_dmx_bit
  pop reg_dmx_byte
  sei
  ret
