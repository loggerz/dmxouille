
    ; === definitions ===
.equ	KPDD = DDRE
.equ	KPDO = PORTE
.equ	KPDI = PINE

.equ	KPD_DELAY = 5	; msec, debouncing keys of keypad

.def	wr0 = r2		; detected col in hex
.def	wr1 = r1		; detected line in hex
.def	mask = r14		; row mask indicating which row has been detected in bin

    ; === interrupt service routines ===
KPD_isr_ext_int4:
    sbi    PORTB,0
    WAIT_MS 50
    cbi    PORTB,0

    _LDI	wr1, 0x00<<2		; detect row 1
    _LDI	mask, 0b00010000
    rjmp	column_detect
    ; no reti (grouped in isr_return)

KPD_isr_ext_int5:
    sbi    PORTB,0
    WAIT_MS 50
    cbi    PORTB,0
    
    _LDI	wr1, 0x01<<2		; detect row 2
    _LDI	mask, 0b00100000
    rjmp	column_detect
    ; no reti (grouped in isr_return)

KPD_isr_ext_int6:
    sbi    PORTB,0
    WAIT_MS 50
    cbi    PORTB,0
    
    _LDI	wr1, 0x02<<2		; detect row 3
    _LDI	mask, 0b01000000
    rjmp	column_detect
    ; no reti (grouped in isr_return)

KPD_isr_ext_int7:
    sbi    PORTB,0
    WAIT_MS 50
    cbi    PORTB,0
    
    _LDI	wr1, 0x03<<2		; detect row 4
    _LDI	mask, 0b10000000
    rjmp	column_detect
    ; no reti (grouped in isr_return)


column_detect:
    OUTI	KPDO,0xff	; bit0-3 driven high
col3:
    WAIT_MS	KPD_DELAY
    OUTI	KPDO,0xf7	; check column 3
    WAIT_MS	KPD_DELAY
    in		w,KPDI
    and		w,mask
    tst		w
    brne	col2
    _LDI	wr0,0x00
    rjmp	isr_return
    
col2:
    WAIT_MS	KPD_DELAY
    OUTI	KPDO,0xfb	; check column 2
    WAIT_MS	KPD_DELAY
    in		w,KPDI
    and		w,mask
    tst		w
    brne	col1
    _LDI	wr0,0x01
    rjmp	isr_return
    
col1:
    WAIT_MS	KPD_DELAY
    OUTI	KPDO,0xfd	; check column 1
    WAIT_MS	KPD_DELAY
    in		w,KPDI
    and		w,mask
    tst		w
    brne	col0
    _LDI	wr0,0x02
    rjmp	isr_return
    
col0:
    WAIT_MS	KPD_DELAY
    OUTI	KPDO,0xfe	; check column 0
    WAIT_MS	KPD_DELAY
    in		w,KPDI
    and		w,mask
    tst		w
    brne	err_row0
    _LDI	wr0,0x03
    rjmp	isr_return

    
    err_row0:			; debug purpose and filter residual glitches		

    rjmp	isr_return
    ; no reti (grouped in isr_return)

isr_return:
    sbi    PORTB,0
    OUTI	KPDO,0xf0
    WAIT_MS 10
    cbi    PORTB,0

    ; decode
    clr		_w
    add		_w, wr1
    add		_w, wr0

    LOOKUP	c1, _w, KeypadLUT

    tst c1
    brne isr_return_non_zero
    mov c2, c1
    isr_return_non_zero:

    call DMX_load_value
	reti

KPD_init:
	OUTI	KPDD,0x0f		; bit4-7 pull-up and bits0-3 driven low
	OUTI	KPDO,0xf0		;>(needs the two lines)
	OUTI	EIMSK,0xf0		; enable INT4-INT7
	OUTI	EICRB,00000000	;>at low level
    ret


KeypadLUT:
;    0    1    2    3
.db	0x1, 0x2, 0x3, 0xa	;0
.db 0x4, 0x5, 0x6, 0xb  ;1
.db	0x7, 0x8, 0x9, 0xc  ;2
.db	0xf, 0x0, 0xe, 0xd	;3
